package apis

import (
	"fmt"
	"git.championtek.com.tw/go/eagleeye/https/requests"
	"git.championtek.com.tw/go/eagleeye/https/responses"
	commonResponses "git.championtek.com.tw/go/responses"
	"git.championtek.com.tw/go/sego"
	"github.com/kataras/iris"
	"log"
	"path"
	"runtime"
)

type SearchAPIs struct {
}

var segmenter = sego.Segmenter{}

type Segment struct {
	Text string `json:"text"`
	Pos  string `json:"pos"`
	Freq int    `json:"frequency"`
}

func init() {
	_, currentFilePath, _, _ := runtime.Caller(0)
	dictFilePath := path.Join(path.Dir(currentFilePath), "dictionary.txt")
	fmt.Println("config file path: ", dictFilePath)
	segmenter.LoadDictionary(dictFilePath)
}

func (s *SearchAPIs) SearchQuery(ctx iris.Context) {
	var searchQuery = requests.Conditions{}
	err := ctx.ReadJSON(&searchQuery)
	if err != nil && !iris.IsErrPath(err) {
		rs := commonResponses.Responses{
			Ctx:        ctx,
			HeaderCode: iris.StatusUnprocessableEntity,
			Status:     responses.ParseQueryStringFailed,
			Message:    err,
		}
		rs.WriteJSON()
		return
	}

	fmt.Println("query string: ", searchQuery.Query)
	fmt.Println("keyword string: ", searchQuery.Keywords)
	fmt.Println("sort string: ", searchQuery.Sort)

	segments := segmenter.Segment([]byte(searchQuery.Query))

	for i:= 0; i < len(segments); i++ {
		segment := segments[i]
		log.Printf("%s/%s/%d", segment.Token().Text(), segment.Token().Pos(), segment.Token().Frequency())
		pos := segment.Token().Pos()
		if pos == "r" || pos == "v" || pos == "x" || pos == "uj" {
			segments = append(segments[:i], segments[i+1:]...)
			i-- //form the remove item index to start iterate next item
		}
	}

	segStr := sego.SegmentsToString(segments, false)

	//ss := []*Segment{}
	//
	//for _, segment := range segments {
	//	ss = append(ss, &Segment{Text: segment.Token().Text(), Pos: segment.Token().Pos()})
	//}

	rs := commonResponses.Responses{
		Ctx:        ctx,
		HeaderCode: iris.StatusOK,
		Status:     responses.ParseQueryStringSuccess,
		Data:       segStr,
	}
	rs.WriteJSON()
}
