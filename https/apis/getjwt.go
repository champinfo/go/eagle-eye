package apis

import (
	"git.championtek.com.tw/go/eagleeye/https/middlewares"
	commonResponses "git.championtek.com.tw/go/responses"
	"github.com/kataras/iris"
	"log"
)

func GetJWT(ctx iris.Context) {
	var oneDay int64
	oneDay = 1 * 86400

	type User struct {
		Email string `json:"email"`
	}

	user := User{}

	if err := ctx.ReadJSON(&user); err != nil {
		ctx.Application().Logger().Println("read json error: ", err)
		rs := commonResponses.Responses{
			Ctx:        ctx,
			HeaderCode: iris.StatusBadRequest,
			Status:     commonResponses.JWTTokenNotExist,
			Message:    err,
		}
		rs.WriteJSON()
		return
	}

	token, err := middlewares.GenerateToke(user.Email, oneDay)
	if err != nil {
		rs := commonResponses.Responses{
			Ctx:        ctx,
			HeaderCode: iris.StatusInternalServerError,
			Status:     commonResponses.JWTUnknownError,
			Message:    err,
		}
		rs.WriteJSON()
		return
	}

	ctx.Header("Authorization", "bearer "+token.Raw)
	if _, err := ctx.JSON("token: " + token.Raw); err != nil {
		log.Fatalln("response json error: ", err)
	}
}
