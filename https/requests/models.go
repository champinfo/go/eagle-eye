package requests

const ()

type Conditions struct {
	//搜尋的字串
	Query string `json:"query"`
	//關鍵字
	Keywords string `json:"keywords"`
	//排序
	Sort sort `json:"sort"`
	//進階設定
	//Terms			[]struct{
	//	SortType	int	`json:"sortType"`
	//}	`json:"terms"`
}

type SortType int

const (
	Stars = iota
	Distance
)

//var sortTypes = [...]string {
//	"Stars",
//	"Distance",
//}

type sort struct {
	//排序型態
	Type SortType `json:"sortType" default:"0"`
	//是否為遞減
	IsDescendant bool `json:"isDescendant" default:"true"`
}
