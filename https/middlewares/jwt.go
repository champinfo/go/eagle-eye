package middlewares

import (
	config "git.championtek.com.tw/go/eagleeye/configs"
	"git.championtek.com.tw/go/responses"
	"git.championtek.com.tw/go/ticketinspector"
	"github.com/dgrijalva/jwt-go"
	"github.com/kataras/iris"
	"log"
)

func init() {
	tiConfig := ticketinspector.Config{Secret: config.SysConfig.JWT.Secret}
	if err := ticketinspector.Ti.Init(&tiConfig); err != nil {
		log.Fatal("initialize ticket inspector error: ", err)
	}
}
func GenerateToke(data string, expire int64) (*jwt.Token, error) {
	claims := ticketinspector.PublicClaims{
		Audience:    config.SysConfig.JWT.Audience,
		Subject:     config.SysConfig.JWT.Subject,
		Email:       data,
		PhoneNumber: config.SysConfig.JWT.PhoneNumber,
		Issuer:      config.SysConfig.JWT.Issuer,
		Duration:    expire,
	}
	return ticketinspector.Ti.GenerateToken(&claims)
}

func ValidateToken(ctx iris.Context) {
	tokenRaw, err := ticketinspector.Ti.RetrieveTokenFromHeader(ctx)
	if err != nil {
		rs := responses.Responses{
			Ctx:        ctx,
			HeaderCode: iris.StatusBadRequest,
			Status:     responses.JWTTokenNotExist,
			Message:    err,
		}
		rs.WriteJSON()
		return
	}

	token, _, err := ticketinspector.Ti.ValidateToken(tokenRaw, true)
	if token == nil && err != nil {
		rs := responses.Responses{
			Ctx:        ctx,
			HeaderCode: iris.StatusBadRequest,
			Status:     responses.JWTParseTokenError,
			Message:    err,
		}
		rs.WriteJSON()
		return
	}
	ctx.Next()
}
