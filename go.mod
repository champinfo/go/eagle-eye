module git.championtek.com.tw/go/eagleeye

go 1.12

require (
	git.championtek.com.tw/go/dbmanager v1.0.6
	git.championtek.com.tw/go/responses v1.0.3
	git.championtek.com.tw/go/sego v1.0.5
	git.championtek.com.tw/go/ticketinspector v1.0.6
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/iris-contrib/middleware v0.0.0-20190816193017-7838277651e8
	github.com/issue9/assert v1.3.3 // indirect
	github.com/kataras/iris v11.1.1+incompatible
)
