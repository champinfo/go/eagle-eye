package routes

import (
	databases "git.championtek.com.tw/go/dbmanager"
	config "git.championtek.com.tw/go/eagleeye/configs"
	"log"
)

func NewDBManager() {
	dbConfig := databases.Config{
		Driver:             config.SysConfig.Database.Connection,
		UserName:           config.SysConfig.Database.UserName,
		Password:           config.SysConfig.Database.Password,
		Host:               config.SysConfig.Database.Host,
		Port:               config.SysConfig.Database.Port,
		DBName:             config.SysConfig.Database.Name,
		IsDebug:            true,
		MaxIdleConnections: config.SysConfig.Database.MaxIdleConnections,
		MaxOpenConnections: config.SysConfig.Database.MaxOpenConnections,
	}
	if err := databases.Mgr.Init(&dbConfig); err != nil {
		log.Fatalln("create database manager error: ", err)
	}
}
