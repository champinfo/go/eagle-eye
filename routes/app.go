package routes

import (
	"fmt"
	config "git.championtek.com.tw/go/eagleeye/configs"
	"git.championtek.com.tw/go/eagleeye/https/apis"
	"git.championtek.com.tw/go/eagleeye/https/middlewares"
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"
	"log"
)

type Routes struct {
	service APIService
}

func NewRoutes(service APIService) *Routes {
	return &Routes{service: service}
}

func (rt *Routes) ApplyRoutes() {
	fmt.Println(config.SysConfig.Cors.AllowOrigins)

	app, err := rt.service.Application()
	if err != nil {
		log.Fatal(err.Error())
	}

	crs := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"HEAD", "POST", "GET", "PATCH", "DELETE"},
		AllowedHeaders:   []string{"*"},
		ExposedHeaders:   []string{"Authorization"},
		AllowCredentials: true,
		Debug:            true,
	})
	apiPath := fmt.Sprintf("/%s/%s", config.SysConfig.Host.APIPath, config.SysConfig.Host.APIVersion)
	v1 := app.Party(apiPath, crs).AllowMethods(iris.MethodOptions)
	{
		v1.Get("/getjwt", apis.GetJWT)

		search := v1.Party("/search", middlewares.ValidateToken)
		searchAPIs := apis.SearchAPIs{}
		search.Post("/query", searchAPIs.SearchQuery)
	}
}
