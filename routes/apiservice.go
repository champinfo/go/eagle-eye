package routes

import (
	"errors"
	"fmt"
	"git.championtek.com.tw/go/dbmanager"
	config "git.championtek.com.tw/go/eagleeye/configs"
	"github.com/kataras/iris"
	"github.com/kataras/iris/middleware/logger"
	"os"
	"path"
	"strings"
	"time"
)

const (
	deleteFileOnExist = true
	logFolder         = "logs"
)

type APIService interface {
	NewService()
	Configuration()
	Application() (*iris.Application, error)
	Println(msg ...interface{})
	Fatal(msg ...interface{})
	Run() error
}

var Service APIService

type service struct {
	app *iris.Application
}

func init() {
	Service = &service{}
}

func (s *service) NewService() {
	app := iris.Default()
	s.app = app
	rt := NewRoutes(Service)
	rt.ApplyRoutes()
}

func (s *service) Configuration() {
	s.app.Use(iris.Cache304(time.Duration(config.SysConfig.CacheRefreshEvery) * time.Second))

	s.app.Logger().SetLevel("debug")
	requestLog, loggerClose := s.newRequestLogger()
	s.app.Use(requestLog)

	iris.RegisterOnInterrupt(func() {
		if err := databases.Mgr.Close(); err != nil {
			s.Fatal("close database manager error: ", err)
		}
		if err := loggerClose(); err != nil {
			s.Fatal("close logger error: ", err)
		}
	})
}

func (s *service) Application() (*iris.Application, error) {
	if s.app == nil {
		return nil, errors.New("the iris application is nil")
	}
	return s.app, nil
}

func (s *service) Println(msg ...interface{}) {
	s.app.Logger().Println(msg)
}

func (s *service) Fatal(msg ...interface{}) {
	s.app.Logger().Fatal(msg)
}

func (s *service) Run() error {
	return s.app.Run(
		iris.Addr(":"+config.SysConfig.Network.Port),
		iris.WithOptimizations,
		//iris.WithoutServerError(iris.ErrServerClosed),
	)
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
func (s *service) todayFileName() string {
	today := time.Now()
	formatted := fmt.Sprintf("%d-%02d-%02d.txt", today.Year(), today.Month(), today.Day())
	return path.Join(logFolder, formatted)
}

func (s *service) newLogFile() *os.File {
	fileName := s.todayFileName()
	if _, err := os.Stat(logFolder); os.IsNotExist(err) {
		err = os.Mkdir(logFolder, os.ModePerm)
		if err != nil {
			s.Fatal("create logs folder error: ", err)
		}
	}

	f, err := os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		s.Fatal("open log file error: ", err)
	}
	return f
}

var excludeExtensions = [...]string{
	".js",
	".css",
	".jpg",
	".jpeg",
	".png",
	".ico",
	".svg",
}

func (s *service) newRequestLogger() (h iris.Handler, close func() error) {
	close = func() error {
		return nil
	}

	c := logger.Config{
		Status:  true,
		IP:      true,
		Method:  true,
		Path:    true,
		Query:   true,
		Columns: true,
	}

	logFile := s.newLogFile()
	fmt.Println(logFile.Name())

	close = func() error {
		err := logFile.Close()
		if deleteFileOnExist {
			err = os.Remove(logFile.Name())
		}
		return err
	}
	c.LogFunc = func(now time.Time, latency time.Duration, status, ip, method, path string, message interface{}, headerMessage interface{}) {
		output := logger.Columnize(now.Format("2006/01/01 - 12:00:00"), latency, status, ip, method, path, message, headerMessage)
		if _, err := logFile.Write([]byte(output)); err != nil {
			fmt.Println(err)
		}
	}
	c.AddSkipper(func(ctx iris.Context) bool {
		p := ctx.Path()
		for _, ext := range excludeExtensions {
			if strings.HasSuffix(p, ext) {
				return true
			}
		}
		return false
	})
	h = logger.New(c)
	return
}
