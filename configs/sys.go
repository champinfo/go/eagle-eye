package configs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"runtime"
)

var SysConfig = &sysconfig{}

func init() {
	_, currentFilePath, _, _ := runtime.Caller(0)
	configFilePath := path.Join(path.Dir(currentFilePath), "config.json")
	fmt.Println("config file path: ", configFilePath)

	//check the config file exists or not
	info, err := os.Stat(configFilePath)
	if os.IsNotExist(err) || info.IsDir() {
		log.Fatal("config file does not exist")
	}

	b, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		log.Fatal("sys config read error: ", err)
	}
	if err = json.Unmarshal(b, &SysConfig); err != nil {
		log.Fatal("decode json error: ", err)
	}
}

type sysconfig struct {
	CacheRefreshEvery int      `json:"CacheRefreshEvery"`
	Database          database `json:"Database"`
	Host              host     `json:"Host"`
	Network           network  `json:"Network"`
	JWT               jwt      `json:"JWT"`
	Cors              cors     `json:"Cors"`
}

type host struct {
	URL        string `json:"URL"`
	Port       string `json:"Port"`
	APIPath    string `json:"APIPath"`
	APIVersion string `json:"APIVersion"`
}

type database struct {
	Name               string `json:"Name"`
	Connection         string `json:"Connection"`
	UserName           string `json:"UserName"`
	Password           string `json:"Password"`
	Host               string `json:"Host"`
	Port               string `json:"Port"`
	MaxIdleConnections int    `json:"MaxIdleConnections"`
	MaxOpenConnections int    `json:"MaxOpenConnections"`
	IsDebug            bool   `json:"IsDebug"`
}

type network struct {
	Protocol string `json:"Protocol"`
	Host     string `json:"Host"`
	Port     string `json:"Port"`
}

type cors struct {
	AllowOrigins []string `json:"AllowOrigins"`
}

type jwt struct {
	Secret             string `json:"Secret"`
	Audience           string `json:"Audience"`
	Subject            string `json:"Subject"`
	Email              string `json:"Email"`
	PhoneNumber        string `json:"PhoneNumber"`
	Issuer             string `json:"Issuer"`
	Duration           int64  `json:"Duration"`
	JWTExpireDate      int64  `json:"JWTExpireDate"`
	JWTShortExpireTime int64  `json:"JWTShortExpireTime"`
}
