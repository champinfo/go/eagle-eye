package main

import (
	"git.championtek.com.tw/go/eagleeye/routes"
	"runtime"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	//instantiate dbmanager instance
	routes.NewDBManager()

	//instantiate web service instance
	routes.Service.NewService()
	routes.Service.Configuration()

	if err := routes.Service.Run(); err != nil {
		routes.Service.Fatal("start service error: ", err)
	}
}
